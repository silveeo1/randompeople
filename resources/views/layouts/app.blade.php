<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Random People</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="/css/app.css">
        @livewireStyles
    </head>

    <body class="theme-dark bg-background-primary text-copy-primary">
        @livewire('header')
        @yield('content')
        <script src="/js/app.js"></script>
        @livewireScripts
    </body>

    <footer class="border-t border-gray-600 p-4">
        <div class="container mx-auto flex items-center flex-col justify-center space-y-4">
            <div class="block">
                <a href="https://kevinhood.dev/">&copy; {{ date('Y') }} - Designed for fun Kevin Hood</a>
            </div>

            <div class="text-xs">
                <a href="https://randomuser.me/">Api provided by https://randomuser.me/</a>
            </div>
        </div>
    </footer>
</html>
