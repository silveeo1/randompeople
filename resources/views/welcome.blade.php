@extends('layouts.app')

@section('content')

@livewire('modal')

@livewire('random-person')

@livewire('contact-form')

@endsection
