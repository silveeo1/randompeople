<div class="p-4 pb-2" wire:init="GetPerson">
    <div class="container mx-auto flex items-center justify-between">
        {{-- Profile Image --}}
        <img src="{{ $profilePhoto }}" alt="{{ $profileName }}" class="rounded-full mt-4">

        {{-- Profile Links --}}
        <ul class="inline-grid grid-cols-2 gap-4 md:grid-flow-col md:grid-cols-6 md:gap-8 text-center">
            <li> {{-- Person Icon --}}
                <button wire:click="ShowUserDescription" class="w-10 hover:text-gray-500">
                    <svg class="fill-current hover:fill-gray-500" viewBox="0 0 20 20">
                        <path d="M7.725 2.146c-1.016.756-1.289 1.953-1.239 2.59.064.779.222 1.793.222 1.793s-.313.17-.313.854c.109 1.717.683.976.801 1.729.284 1.814.933 1.491.933 2.481 0 1.649-.68 2.42-2.803 3.334C3.196 15.845 1 17 1 19v1h18v-1c0-2-2.197-3.155-4.328-4.072-2.123-.914-2.801-1.684-2.801-3.334 0-.99.647-.667.932-2.481.119-.753.692-.012.803-1.729 0-.684-.314-.854-.314-.854s.158-1.014.221-1.793c.065-.817-.398-2.561-2.3-3.096-.333-.34-.558-.881.466-1.424-2.24-.105-2.761 1.067-3.954 1.929z"/>
                    </svg>
                </button>
            </li>
            <li> {{-- Login Icon --}}
                <button wire:click="ShowUserLogin" class="w-10 hover:text-gray-500">
                    <svg class="fill-current hover:fill-gray-500" viewBox="0 0 20 20">
                        <path d="M17.691 4.725c-.503-2.977-3.22-4.967-6.069-4.441C8.772.809 6.366 3.1 6.869 6.079c.107.641.408 1.644.763 2.365l-5.175 7.723c-.191.285-.299.799-.242 1.141l.333 1.971a.612.612 0 0 0 .7.514l1.516-.281c.328-.059.744-.348.924-.639l2.047-3.311.018-.022 1.386-.256 2.39-3.879c.785.139 1.912.092 2.578-.031 2.848-.526 4.087-3.67 3.584-6.649zm-2.525 1.527c-.784 1.17-1.584.346-2.703-.475-1.119-.818-2.135-1.322-1.352-2.492.784-1.17 2.326-1.455 3.447-.635 1.12.819 1.391 2.432.608 3.602z"/>
                    </svg>
                </button>
            </li>
            <li> {{-- Email Icon --}}
                <button wire:click="ShowUserEmail" class="w-10 hover:text-gray-500">
                    <svg class="fill-current hover:fill-gray-500" viewBox="0 0 20 20">
                        <path d="M1.574 5.286l7.5 4.029c.252.135.578.199.906.199.328 0 .654-.064.906-.199l7.5-4.029c.489-.263.951-1.286.054-1.286H1.521c-.897 0-.435 1.023.053 1.286zm17.039 2.203l-7.727 4.027c-.34.178-.578.199-.906.199s-.566-.021-.906-.199-7.133-3.739-7.688-4.028C.996 7.284 1 7.523 1 7.707V15c0 .42.566 1 1 1h16c.434 0 1-.58 1-1V7.708c0-.184.004-.423-.387-.219z"/>
                    </svg>
                </button>
            </li>
            <li> {{-- Map icon --}}
                <button wire:click="ShowUserLocation" class="w-10 hover:text-gray-500">
                    <svg class="fill-current hover:fill-gray-500" viewBox="0 0 20 20">
                        <path d="M19.447 3.718l-6-3a1 1 0 0 0-.895 0l-5.63 2.815-5.606-1.869A1 1 0 0 0 0 2.613v13.001c0 .379.214.725.553.894l6 3a1.006 1.006 0 0 0 .894 0l5.63-2.814 5.606 1.869a.999.999 0 0 0 1.316-.949V4.612a.996.996 0 0 0-.552-.894zM8 5.231l4-2v11.763l-4 2V5.231zM2 4l4 1.333v11.661l-4-2V4zm16 12.227l-4-1.334V3.231l4 2v10.996z"/>
                    </svg>
                </button>
            </li>
            <li> {{-- Phone icon --}}
                <button wire:click="ShowUserPhone" class="w-10 hover:text-gray-500">
                    <svg class="fill-current hover:fill-gray-500" viewBox="0 0 20 20">
                        <path d="M11.229 11.229c-1.583 1.582-3.417 3.096-4.142 2.371-1.037-1.037-1.677-1.941-3.965-.102-2.287 1.838-.53 3.064.475 4.068 1.16 1.16 5.484.062 9.758-4.211 4.273-4.274 5.368-8.598 4.207-9.758-1.005-1.006-2.225-2.762-4.063-.475-1.839 2.287-.936 2.927.103 3.965.722.725-.791 2.559-2.373 4.142z"/>
                    </svg>
                </button>
            </li>
            <li> {{-- Birthday icon --}}
                <button wire:click="ShowUserBirthday" class="w-10 hover:text-gray-500">
                    <svg class="fill-current hover:fill-gray-500" viewBox="0 0 20 20">
                        <path d="M9.584 6.036c1.952 0 2.591-1.381 1.839-2.843-.871-1.693 1.895-3.155.521-3.155-1.301 0-3.736 1.418-4.19 3.183-.339 1.324.296 2.815 1.83 2.815zm5.212 8.951l-.444-.383a1.355 1.355 0 0 0-1.735 0l-.442.382a3.326 3.326 0 0 1-2.174.801 3.325 3.325 0 0 1-2.173-.8l-.444-.384a1.353 1.353 0 0 0-1.734.001l-.444.383c-1.193 1.028-2.967 1.056-4.204.1V19a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-3.912c-1.237.954-3.011.929-4.206-.101zM10 7c-7.574 0-9 3.361-9 5v.469l1.164 1.003a1.355 1.355 0 0 0 1.735 0l.444-.383a3.353 3.353 0 0 1 4.345 0l.444.384c.484.417 1.245.42 1.735-.001l.442-.382a3.352 3.352 0 0 1 4.346-.001l.444.383c.487.421 1.25.417 1.735 0L19 12.469V12c0-1.639-1.426-5-9-5z"/>
                    </svg>
                </button>
            </li>
        </ul>
    </div>

    <div class="container mx-auto py-8 space-y-4">
        <h1 class="text-bold text-5xl">
            {{ $profileName }}
        </h1>

        <div>
            {{ $description }}
        </div>
    </div>
</div>
