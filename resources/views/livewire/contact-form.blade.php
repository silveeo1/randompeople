<div class="container mx-auto flex items-center border-t border-gray-600 py-2">
    <div class="mx-auto p-4 w-10/12 md:w-1/2">

        <h1 class="text-4xl text-bold w-auto pb-4">Contact This Person</h1>

        <form wire:submit.prevent="submit" class="block m-auto space-y-4">

            <input type="text" id="name" placeholder="Name" class="block w-full px-2 border-b-2 border-gray-600 bg-transparent outline-none" wire:model.lazy="name">
            @error('name')
                <span class="error text-red-600">
                    {{ $message }}
                </span>
            @enderror

            <input type="text" id="email" placeholder="Email" class="block w-full px-2 border-b-2 border-gray-600 bg-transparent outline-none" wire:model.lazy="email">
            <div>
            @error('email')
                <span class="error text-red-600">
                        {{ $message }}
                </span>
            @enderror
            </div>

            <textarea id="text" placeholder="How can I help you?" rows="4" class="block w-full px-2 border-2 border-gray-600 bg-transparent outline-none" wire:model.lazy="text"></textarea>
            @error('text')
                <span class="error text-red-600">
                        {{ $message }}
                </span>
            @enderror

            @if (session()->has('message'))
                <div class="alert alert-success text-green-600 w-full px-2">
                    {{ session('message') }}
                </div>
            @endif

            <button type="#" class="block m-auto w-1/2 md:w-1/4 border-2 border-gray-600 bg-transparent outline-gray-600 hover:outline-gray-300 hover:border-gray-300 hover:text-gray-300">
                Submit
            </button>

        </form>
    </div>
</div>
