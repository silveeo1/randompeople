<div class="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
    <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50">
    </div>

    <div class="modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">

        <div class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
            <svg class="fill-current text-white"width="18" height="18" viewBox="0 0 18 18">
                    <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
            </svg>
            <span class="text-sm">(Esc)</span>
        </div>

        <!-- Add margin if you want to see some of the overlay behind the modal-->
        <div class="modal-content py-4 text-left px-6">
            <!--Title-->
            <div class="flex justify-between items-center pb-3">
                <p class="text-2xl font-bold text-black">About this project.</p>
                <div class="modal-close cursor-pointer z-50">
                    <svg class="fill-current text-black" width="18" height="18" viewBox="0 0 18 18">
                        <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
                    </svg>
                </div>
            </div>
            <!--Modal Body-->
            <p class="text-black">
                This is an embelishment on a coding test I was timed on.  The original was horrible and I wanted to rebuild from scratch.  I'm pretty happy with the way this version turned out.  It's clean and functional.  This was the first coding test I've ever taken in trying to break into this industry.  I crammed two new frameworks in 12 hours to take the original test and it showed.  This is the product of learning from the first attempt.
            </p>
            </br>
            <p class="text-black">
                Future plans for this project will be to add a light/dark mode toggle switch.
            </p>
            <!--Close Button-->
            <div class="flex justify-end pt-2">
                <button class="modal-close px-4 bg-indigo-500 p-3 rounded-lg text-white hover:bg-indigo-400">Neat!</button>
            </div>
        </div>
    </div>
</div>
