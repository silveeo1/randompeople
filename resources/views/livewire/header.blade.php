<header class="border-b border-gray-600 p-4">
    <nav class="container mx-auto flex items-center justify-between">
        <a href="/" class="text-xl">
            Random People
        </a>
        <ul class="flex flex-row space-x-8 text-sm">
            <li>
                <div class="relative inline-block w-10 mr-2 align-middle select-none transition duration-200 ease-in">
                    <input type="checkbox" name="toggle" id="toggle" class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer"/>
                    <label for="toggle" class="toggle-label block overflow-hidden h-6 rounded-full bg-gray-300 cursor-pointer"></label>
                </div>
                <label for="toggle" class="text-xs text-copy-primary">Toggle Light/Dark Mode</label>
            </li>
            <li><a class="hover:underline" href="/">New Random Person</a></li>
            <li><button class="modal-open bg-none border-none cursor-pointer outline-none hover:underline">About This Project</button></li>
            <li><a class="hover:underline" href="https://kevinhood.dev">Back to KevinHood.dev</a></li>
        </ul>
    </nav>
</header>
