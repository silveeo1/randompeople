module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
        colors: {
            background: {
                primary: 'var(--bg-background-primary)',
            },
            copy: {
                primary: 'var(--text-primary)',
            }
        },
    },
  },
  variants: {},
  plugins: [],
}
