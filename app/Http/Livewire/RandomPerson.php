<?php

namespace App\Http\Livewire;
use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Support\Facades\Http;

class RandomPerson extends Component
{
    public $person;
    public $profilePhoto;
    public $profileName;
    public $description;
    public $location;

    //the main http request through livewire magic that uses ajax
    public function GetPerson()
    {
        $this->person = Http::get('http://randomuser.me/api/?nat=us')->json()['results'][0];
        $this->profilePhoto = $this->person['picture']['large'];
        $this->profileName = $this->person['name']['first'] . " " . $this->person['name']['last'];
        $this->description = "Hello, my name is " . $this->person['name']['first'] . ", and I live in " . $this->person['location']['city'] . ", " . $this->person['location']['state'] . ".";
    }

    //this handles the first display with a bunch of concantenated strings and variable for user to read profile information
    public function ShowUserDescription()
    {
        $this->description = "Hello, my name is" . " " . $this->person['name']['first'] . "and I live in " . $this->person['location']['city'] . ", " . $this->person['location']['state'] . ".";
    }

    //i would have like to embed a map with their gps coordinates, but I just listed their address as a concantenated string.
    public function ShowUserLocation()
    {
        $this->description = $this->person['location']['street']['number'] . " " . $this->person['location']['street']['name'] . " " . $this->person['location']['city'] . ", " . $this->person['location']['state'] . " " . $this->person['location']['postcode'] . " " . $this->person['location']['country'];

    }
    //function to display email
    public function ShowUserEmail()
    {
        $this->description = "My email address is  " . $this->person['email'];
    }

    //function to display phone
    public function ShowUserPhone()
    {
        $this->description = "My phone number is "  . $this->person['phone'];
    }

    //function to display birthday
    public function ShowUserBirthday()
    {
        $this->description = "I was born on  " . Carbon::parse($this->person['dob']['date'])->format('F d, Y');
    }

    //function to display user's login information
    public function ShowUserLogin()
    {
        $this->description = "Username:  " . $this->person['login']['username'];/*  . " Password:  " . $this->person['login']['password']; */


    }

    public function render()
    {
        return view('livewire.random-person');
    }
}
