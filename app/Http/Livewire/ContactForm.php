<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ContactForm extends Component
{
    public $name;
    public $email;
    public $text;

    protected $rules = [
        'name' => 'required|min:6',
        'email' => 'required|email',
        'text' => 'required|min:10',

    ];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'required|min:6',
            'email' => 'required|email',
            'text' => 'required|min:10',
        ]);
    }

    public function submit()
    {
        $this->validate();
        $this->resetForm();

        session()->flash('message', 'If this was a real person, they would appreciate the message you just sent them.');

    }

    private function resetForm()
    {
        $this->name = '';
        $this->email = '';
        $this->text = '';
    }

    public function render()
    {
        return view('livewire.contact-form');
    }
}
